cmake_minimum_required(VERSION 3.6.0)
project(itk-dcs-sw)
file(GLOB EXCLUDES LIST_DIRECTORIES true RELATIVE ${CMAKE_SOURCE_DIR} "*opc-server")
list(APPEND EXCLUDES quasar)
set(DISABLE_PACKAGES ${EXCLUDES})
message(INFO " Disabled packages: ${DISABLED_PACKAGES}")
find_package(TDAQ)
tdaq_project(itk-dcs-sw 11.2.1 USES tdaq 11.2.1)


