# ITK DCS SW

This is a collection of software for ITK Common DCS.

### Installation

1. Checkout itk-dcs-sw package
```
git clone https://gitlab.cern.ch/atlas-itk-dcs/itk-dcs-sw.git 
```

2. Change to the itk-dcs-sw directory
```
cd itk-dcs-sw
```

3. Checkout the packages that you need for your setup
```
git clone https://gitlab.cern.ch/atlas-itk-dcs/elmb_configurator.git
git clone https://gitlab.cern.ch/atlas-itk-dcs/elmb_opc_client.git
```

4. Setup the environment
```
source setup.sh
```

5. Make a build directory
```
mkdir build
```

6. Change to the build directory
```
cd build
```

7. Config the release for building
```
cmake ..
```

8. Compile and install the software
```
make -j && make install
```

### Links

 * ATLAS TDAQ is available in the following [Link](https://gitlab.cern.ch/atlas-tdaq-software)

